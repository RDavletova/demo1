import { LoginVikunja } from './services/index';

const apiProvider = () => ({
  login: () => new LoginVikunja(),
});

export { apiProvider };
