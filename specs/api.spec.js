import { apiProvider } from '../framework';
//import { Severity } from "jest-allure/dist/Reporter";

test('Provider demo', async () => {
    reporter          
        .description("Provider demo")          
        //.severity(Severity.Critical)      
        .feature('Feature.Betting')          
        .story("BOND-007");      
        reporter.startStep("Готовим данные");      // expect that it's fancy      
        reporter.endStep();

    const body = {
        username: 'demo',
        password: 'demo',
    };
    const r = await apiProvider().login().post(body);
    expect(r.status).toBe(200);
});
